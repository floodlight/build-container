FROM fedora:latest

# FLATPAK STUFF
RUN dnf install -y flatpak flatpak-builder
RUN flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
RUN flatpak install -y flathub org.gnome.Platform//41
RUN flatpak install -y flathub org.gnome.Sdk//41


# MINGW STUFF

# Set up a workspace to use when building stuff
WORKDIR /app
COPY . /app

# Install various build tools and essential libraries
RUN dnf install -y 	git make ninja-build vala gettext autoconf-archive wget xz \
					gcc-c++ wine nasm meson \
					mingw64-gcc mingw64-pkg-config mingw32-nsis python3-pefile \
					mingw64-glib2 mingw64-json-glib mingw64-cairo mingw64-gtk3 \
					mingw64-adwaita-icon-theme mingw64-hicolor-icon-theme \
					mingw64-libsoup \
					mingw64-gdk-pixbuf mingw64-gstreamer1 \
					mingw64-gstreamer1-plugins-base \
					mingw64-gstreamer1-plugins-good \
					mingw64-gstreamer1-plugins-bad-free

# NOTE: The GStreamer bad-free plugins are required on Windows because we need
# the wasapi plugin to play sound on Windows.

# Install some other Windows libraries that aren't available in the Fedora repo
RUN /app/install-deps.sh
