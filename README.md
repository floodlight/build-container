A Docker container for building Floodlight. The container is based on the
official Fedora image, and it comes with Flatpak, org.gnome.Sdk//41, the
MinGW cross-compiler, and a bunch of MinGW packages.

# How to Use
Simply use `registry.gitlab.com/floodlight/build-container:latest` as the
Docker image for your CI job. All the useful stuff listed above will be
available for that job.

# License
The contents of this repository are provided under the MIT license. See the
COPYING file.
