#!/bin/bash

# Installs Floodlight dependencies that aren't in Fedora's MinGW packages so
# need to be installed from source.

## LIBGEE ##
git clone https://gitlab.gnome.org/GNOME/libgee.git
cd libgee
NOCONFIGURE=yes ./autogen.sh
mingw64-configure
make
make install
cd -

## GST-LIBAV ##
LIBAV=gst-libav-1.14.0
wget https://gstreamer.freedesktop.org/src/gst-libav/$LIBAV.tar.xz
tar -xJf $LIBAV.tar.xz
cd $LIBAV
NOCONFIGURE=yes ./autogen.sh
mingw64-configure
make
make install
cd -

